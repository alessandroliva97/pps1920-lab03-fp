package u03

import u02.Modules.Person
import u02.Modules.Person.Teacher
import u02.Optionals.Option
import u02.Optionals.Option._

import scala.annotation.tailrec

object Lists {

  // A generic linkedlist
  sealed trait List[E]

  // a companion object (i.e., module) for List
  object List {
    case class Cons[E](head: E, tail: List[E]) extends List[E]
    case class Nil[E]() extends List[E]

    def sum(l: List[Int]): Int = l match {
      case Cons(h, t) => h + sum(t)
      case _ => 0
    }

    def append[A](l1: List[A], l2: List[A]): List[A] = (l1, l2) match {
      case (Cons(h, t), l2) => Cons(h, append(t, l2))
      case _ => l2
    }

    @tailrec def drop[A](l: List[A], n: Int): List[A] = (l, n) match {
      case (Cons(_, t), i) if i > 0 => drop(t, n - 1)
      case (_, _) => l
    }

    def flatMap[A, B](l: List[A])(f: A => List[B]): List[B] = l match {
      case Nil() => Nil()
      case Cons(h, t) => append(f(h), flatMap(t)(f))
    }

    def map[A, B](l: List[A])(mapper: A => B): List[B] = flatMap(l)(v => Cons(mapper(v), Nil()))

    def filter[A](l1: List[A])(pred: A => Boolean): List[A] = flatMap(l1)(v => pred(v) match {
      case true => Cons(v, Nil())
      case _ => Nil()
    })

    def max(l: List[Int]): Option[Int] = l match {
      case Nil() => None()
      case _ =>
        @tailrec def recursiveMax(l: List[Int], max: Int): Option[Int] = l match {
          case Cons(h, t) => recursiveMax(t, if (h > max) h else max)
          case Nil() => Some(max)
        }
        recursiveMax(l, Int.MinValue)
    }

    def getTeacherCourses(l: List[Person]): List[String] = flatMap(l) {
      case Teacher(_, c) => Cons(c, Nil())
      case _ => Nil()
    }

    @tailrec def foldLeft[A, B](l: List[A])(element: B)(f: (B, A) => B): B = l match {
      case Cons(h, t) => foldLeft(t)(f(element, h))(f)
      case _ => element
    }

    def foldRight[A, B](l: List[A])(element: B)(f: (A, B) => B): B = l match {
      case Cons(h, t) => f(h, foldRight(t)(element)(f))
      case _ => element
    }

  }
}

object ListsMain extends App {
  import Lists._
  val l = List.Cons(10, List.Cons(20, List.Cons(30, List.Nil())))
  println(List.sum(l)) // 60
  import List._
  println(append(Cons(5, Nil()), l)) // 5,10,20,30
  println(filter[Int](l)(_ >=20)) // 20,30
}