package u03

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions._
import u03.Lists.List
import u03.Lists.List._

class TestExercise1 {

  val lst: Cons[Int] = Cons(10, Cons(20, Cons(30, Nil())))

  @Test def testDrop(): Unit = {
    assertEquals(Cons(20, Cons(30, Nil())), drop(lst, 1))
    assertEquals(Cons(30, Nil()), drop(lst, 2))
    assertEquals(Nil(), drop(lst, 5))
  }

  @Test def testFlatMap(): Unit = {
    assertEquals(Cons(11, Cons(21, Cons(31, Nil()))),
      flatMap(lst)(v => Cons(v + 1, Nil())))
    assertEquals(Cons(11, Cons(12, Cons(21, Cons(22, Cons(31, Cons(32, Nil())))))),
      flatMap(lst)(v => Cons(v + 1, Cons(v + 2, Nil()))))
  }

  @Test def testMap(): Unit = {
    assertEquals(Cons(Cons(11, Nil()), Cons(Cons(21, Nil()), Cons(Cons(31, Nil()), Nil()))),
      map(lst)(v => Cons(v + 1, Nil())))
    assertEquals(Cons(Cons(11, Cons(12, Nil())), Cons(Cons(21, Cons(22, Nil())), Cons(Cons(31, Cons(32, Nil())), Nil()))),
      map(lst)(v => Cons(v + 1, Cons(v + 2, Nil()))))
  }

  @Test def testFilter(): Unit = {
    val l = List.Cons(10, List.Cons(20, List.Cons(30, List.Nil())))
    assertEquals(Cons(20, Cons(30, Nil())), filter[Int](l)(_ >=20))
  }

}
