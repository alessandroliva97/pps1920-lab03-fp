package u03

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import u03.Lists.List._
import u03.Streams.Stream
import u03.Streams.Stream.constant

class TestExercise6 {

  @Test def testConstant(): Unit = {
    assertEquals(Cons("x", Cons("x", Cons("x", Cons("x", Cons("x", Nil()))))),
      Stream.toList(Stream.take(constant("x"))(5)))
  }

}
