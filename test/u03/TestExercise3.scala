package u03

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import u02.Modules.Person
import u02.Modules.Person.{Student, Teacher}
import u03.Lists.List._

class TestExercise3 {

  @Test def testGetTeacherCourses(): Unit = {
    val lst: Cons[Person] = Cons(Student("Mario", 3), Cons(Teacher("Luigi", "Sistemi"), Cons(Teacher("Wario", "Analisi"), Nil())))
    assertEquals(Cons("Sistemi", Cons("Analisi", Nil())), getTeacherCourses(lst))
  }

}
